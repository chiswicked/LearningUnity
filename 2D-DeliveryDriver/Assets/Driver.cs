using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Driver : MonoBehaviour
{
    Color32 hasPackageColor = new Color32(255, 100, 255, 255);
    Color32 noPackageColor = new Color32(255, 255, 255, 255);

    [SerializeField] float steerSpeed = 200;
    [SerializeField] float defaultMoveSpeed = 20;
    [SerializeField] float slowMoveSpeed = 10;
    [SerializeField] float fastMoveSpeed = 30;
    float moveSpeed;

    bool hasPackage = false;

    SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        moveSpeed = defaultMoveSpeed;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        float steerAmountPerSecond = Input.GetAxis("Horizontal") * steerSpeed * Time.deltaTime;
        float moveAmountPerSecond = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;
        if (moveAmountPerSecond >= 0)
        {
            steerAmountPerSecond = -steerAmountPerSecond;
        }
        transform.Rotate(0, 0, steerAmountPerSecond);
        transform.Translate(0, moveAmountPerSecond, 0);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Package" && !hasPackage)
        {
            hasPackage = true;
            spriteRenderer.color = hasPackageColor;
            Destroy(other.gameObject);
        }

        if (other.tag == "Customer" && hasPackage)
        {
            hasPackage = false;
            spriteRenderer.color = noPackageColor;
        }

        if (other.tag == "Boost")
        {
            if (moveSpeed != fastMoveSpeed)
            {
                if (moveSpeed == slowMoveSpeed)
                {
                    moveSpeed = defaultMoveSpeed;
                }
                else
                {
                    moveSpeed = fastMoveSpeed;
                }
                Destroy(other.gameObject);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        moveSpeed = slowMoveSpeed;
    }
}
