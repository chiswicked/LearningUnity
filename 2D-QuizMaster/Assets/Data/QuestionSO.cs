using UnityEngine;

[CreateAssetMenu(fileName = "QuestionSO", menuName = "Quiz Question", order = 0)]
public class QuestionSO : ScriptableObject
{
    [TextArea(2, 6)]
    [SerializeField] string question = "Enter new question here";
    [SerializeField] string[] answers = new string[4];
    [SerializeField] int correctAnswerIndex;


    public string GetQuestion()
    {
        return question;
    }
    public string GetAnswer(int i)
    {
        return answers[i];
    }
    public int GetCorrectAnswerIndex()
    {
        return correctAnswerIndex;
    }
    public string GetCorrectAnswer()
    {
        return answers[correctAnswerIndex];
    }
}