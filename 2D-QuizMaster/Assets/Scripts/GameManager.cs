using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    Quiz gameScreen;
    WinScreen winScreen;
    // Start is called before the first frame update
    void Start()
    {
        gameScreen = FindObjectOfType<Quiz>();
        winScreen = FindObjectOfType<WinScreen>();
        gameScreen.gameObject.SetActive(true);
        winScreen.gameObject.SetActive(false);
    }

    public void StartNewGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ShowWinScreen()
    {
        gameScreen.gameObject.SetActive(false);
        winScreen.gameObject.SetActive(true);
        winScreen.UpdateWinnings();
    }
}