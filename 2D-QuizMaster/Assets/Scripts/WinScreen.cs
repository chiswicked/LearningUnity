using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WinScreen : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI winningsText;
    Score score;

    private void Start()
    {
        score = FindObjectOfType<Score>();
    }

    public void UpdateWinnings()
    {
        winningsText.text = "Congratulations!\nYou answered " +
            score.GetCorrectAnswers() + "/" + score.GetTotalAnswered() + " correctly.\nThat's " +
            score.GetSuccessRate() + "% success.";
    }
}
