using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    private int correctAnswers;
    private int totalAnswered;

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        correctAnswers = 0;
        totalAnswered = 0;
    }

    public int GetCorrectAnswers()
    {
        return correctAnswers;
    }

    public int GetTotalAnswered()
    {
        return totalAnswered;
    }

    public int GetSuccessRate()
    {
        if (totalAnswered == 0)
        {
            return 0;
        }
        return Mathf.RoundToInt(correctAnswers / (float)totalAnswered * 100);
    }

    public void IncCorrectAnswers()
    {
        correctAnswers++;
    }

    public void IncTotalAnswered()
    {
        totalAnswered++;
    }
}
