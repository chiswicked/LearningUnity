using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private bool active = false;
    public bool answering = true;
    private const float timeToAnswer = 10f;
    private const float timeToReview = 5f;
    private float timeLeft;
    public bool readyForNextQuestion;
    public bool outOfTime;

    public void StartTimer(bool answering)
    {
        active = true;
        readyForNextQuestion = false;
        outOfTime = false;
        this.answering = answering;
        if (answering)
        {
            timeLeft = timeToAnswer;
        }
        else
        {
            timeLeft = timeToReview;
        }
    }

    public float GetTimeLeft()
    {
        if (!active)
        {
            return 1f;
        }

        if (timeLeft <= 0)
        {
            return 0f;
        }

        if (answering)
        {
            return timeLeft / timeToAnswer;
        }
        return timeLeft / timeToReview;
    }

    void Update()
    {
        if (!active)
        {
            return;
        }

        if (timeLeft <= 0)
        {
            if (answering)
            {
                outOfTime = true;
            }
            else
            {
                readyForNextQuestion = true;
            }
            return;
        }

        timeLeft -= Time.deltaTime;
    }
}
