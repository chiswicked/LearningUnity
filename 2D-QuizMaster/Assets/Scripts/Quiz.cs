using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Quiz : MonoBehaviour
{
    [Header("Questions")]
    [SerializeField] TextMeshProUGUI questionText;
    [SerializeField] List<QuestionSO> questions = new List<QuestionSO>();
    QuestionSO currentQuestion;

    [Header("Answers")]
    [SerializeField] GameObject[] answerButtons;
    [Header("Button Colors")]
    [SerializeField] Sprite defaultAnswerSprite;
    [SerializeField] Sprite correctAnswerSprite;

    [Header("Timer")]
    [SerializeField] Image timerImage;
    Timer timer;

    [Header("Score")]
    [SerializeField] TextMeshProUGUI scoreText;
    private Score score;

    Slider progressBar;
    private int noOfQuestions;
    void Start()
    {
        noOfQuestions = questions.Count;
        timer = FindObjectOfType<Timer>();
        score = FindObjectOfType<Score>();
        progressBar = FindObjectOfType<Slider>();
        progressBar.interactable = false;
        progressBar.minValue = 0;
        progressBar.maxValue = noOfQuestions;
        progressBar.wholeNumbers = true;
        PlayNextQuestion();
    }

    private void PlayNextQuestion()
    {
        UpdateScoreText();
        if (questions.Count == 0)
        {
            return;
        }
        SetButtonsInteractable(true);
        SetButtonsDefaultSprites();
        GetRandomQuestion();
        DisplayNextQuestion();
        progressBar.value++;
        timer.StartTimer(true);
    }

    private void PlayFeedback(int i)
    {
        score.IncTotalAnswered();
        timer.StartTimer(false);
        SetButtonsInteractable(false);
        if (i == currentQuestion.GetCorrectAnswerIndex())
        {
            score.IncCorrectAnswers();
            questionText.text = "Correct!";
            Image buttonImage = answerButtons[i].GetComponent<Image>();
            buttonImage.sprite = correctAnswerSprite;
        }
        else
        {
            questionText.text = "Sorry, the correct answer was " + currentQuestion.GetCorrectAnswerIndex() + ".\n" + currentQuestion.GetCorrectAnswer();
        }
        UpdateScoreText();
    }
    private void SetButtonsDefaultSprites()
    {
        for (int i = 0; i < answerButtons.Length; i++)
        {
            Image buttonImage = answerButtons[i].GetComponent<Image>();
            buttonImage.sprite = defaultAnswerSprite;
        }
    }
    private void GetRandomQuestion()
    {
        int i = Random.Range(0, questions.Count);
        currentQuestion = questions[i];
        if (questions.Contains(currentQuestion))
        {
            questions.Remove(currentQuestion);
        }
    }

    private void DisplayNextQuestion()
    {
        questionText.text = currentQuestion.GetQuestion();

        for (int i = 0; i < answerButtons.Length; i++)
        {
            TextMeshProUGUI buttonText = answerButtons[i].GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = currentQuestion.GetAnswer(i);
        }
    }

    public void OnAnswerSelected(int i)
    {
        PlayFeedback(i);
    }

    private void SetButtonsInteractable(bool b)
    {
        for (int i = 0; i < answerButtons.Length; i++)
        {
            answerButtons[i].GetComponent<Button>().interactable = b;
        }
    }

    private void UpdateScoreText()
    {
        scoreText.text = "Success rate: " + GetComponent<Score>().GetSuccessRate() + "%";
    }

    private void Update()
    {
        float timeLeft = timer.GetTimeLeft();
        if (timer.answering)
        {
            timerImage.fillAmount = timeLeft;
            timerImage.fillClockwise = false;
            timerImage.color = new Color(1f, 1f, 1f, 1f);
        }
        else
        {

            timerImage.fillAmount = 1 - timeLeft;
            timerImage.fillClockwise = true;
            timerImage.color = new Color(1f, 1f, 1f, 0.3f);
        }

        if (timer.outOfTime)
        {
            PlayFeedback(-1);
        }
        if (timer.readyForNextQuestion)
        {
            if (questions.Count == 0)
            {
                GameManager gameManager = FindObjectOfType<GameManager>();
                gameManager.ShowWinScreen();
            }
            else
            {
                PlayNextQuestion();
            }
        }
    }
}
