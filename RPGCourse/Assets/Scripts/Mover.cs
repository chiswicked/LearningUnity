using UnityEngine;
using UnityEngine.AI;

public class Mover : MonoBehaviour {
    private NavMeshAgent navMeshAgent;
    private Ray lastRay;

    private void Start() {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            MoveToCursor();
        }
    }

    private void MoveToCursor() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            navMeshAgent.destination = hit.point;
        }
    }
}
