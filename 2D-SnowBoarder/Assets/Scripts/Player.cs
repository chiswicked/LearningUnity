using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] float delay = 1f;
    [SerializeField] ParticleSystem damageEffect;
    [SerializeField] ParticleSystem snowEffect;
    Rigidbody2D rb2d;
    SurfaceEffector2D se2d;
    float torqueAmount = 0.5f;
    float normalSpeed = 15f;
    float boostSpeed = 20f;
    float slowSpeed = 10f;
    bool canMove = true;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        se2d = FindObjectOfType<SurfaceEffector2D>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!canMove)
        {
            return;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb2d.AddTorque(torqueAmount);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            rb2d.AddTorque(-torqueAmount);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            se2d.speed = boostSpeed;
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            se2d.speed = slowSpeed;
        }
        else
        {
            se2d.speed = normalSpeed;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Ground")
        {
            DisableControls();
            damageEffect.Play();
            audioSource.Play();
            Invoke(nameof(ReloadScene), delay);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Ground")
        {
            snowEffect.Play();
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Ground")
        {
            snowEffect.Stop();
        }
    }
    public void DisableControls()
    {
        canMove = false;
    }

    void ReloadScene()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}