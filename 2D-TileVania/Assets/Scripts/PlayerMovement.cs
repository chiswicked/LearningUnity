using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] GameObject arrow;
    [SerializeField] Transform bow;
    private float playerGravity;
    private const float climbSpeed = 2f;
    private bool isClimbing = false;
    private const float runSpeed = 5f;
    private bool isRunning = false;
    private const float jumpSpeed = 10f;
    Vector2 moveInput;
    Collider2D cc2d;
    Collider2D bc2d;
    Rigidbody2D rb2d;
    Animator anim;
    private bool isAlive = true;

    void Start()
    {
        cc2d = GetComponent<CapsuleCollider2D>();
        bc2d = GetComponent<BoxCollider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        playerGravity = rb2d.gravityScale;

    }

    void OnMove(InputValue value)
    {
        if (!isAlive) { return; }
        moveInput = value.Get<Vector2>();
    }
    void OnJump(InputValue value)
    {
        if (!isAlive) { return; }
        if (!bc2d.IsTouchingLayers(LayerMask.GetMask("Ground")) || !value.isPressed)
        {
            return;
        }
        rb2d.velocity += new Vector2(0f, jumpSpeed);
    }

    void OnFire(InputValue value)
    {
        if (!value.isPressed || !isAlive) { return; }

        Instantiate(arrow, bow.position, transform.rotation);

    }

    private void Update()
    {
        // Dying
        if (!isAlive) { return; }
        if (cc2d.IsTouchingLayers(LayerMask.GetMask("Enemies", "Hazards")))
        {
            isAlive = false;
            rb2d.velocity = new Vector2(0f, 15f);
            anim.SetTrigger("Dying");
            FindObjectOfType<GameSession>().ProcessPlayerDeath();
        }

        // Running
        Vector2 playerVelocity = new Vector2(moveInput.x * runSpeed, rb2d.velocity.y);
        rb2d.velocity = playerVelocity;
        isRunning = Mathf.Abs(rb2d.velocity.x) > Mathf.Epsilon;
        anim.SetBool("isRunning", isRunning);
        if (isRunning)
        {
            transform.localScale = new Vector2(Mathf.Sign(rb2d.velocity.x), 1f);
        }

        // Climbing
        if (!cc2d.IsTouchingLayers(LayerMask.GetMask("Climb")))
        {
            rb2d.gravityScale = playerGravity;
            anim.SetBool("isClimbing", false);
            return;
        }

        rb2d.gravityScale = 0f;

        float speed;
        if (cc2d.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            speed = runSpeed;
        }
        else
        {
            speed = climbSpeed;
        }
        Vector2 climbVelocity = new Vector2(moveInput.x * speed, moveInput.y * climbSpeed);
        rb2d.velocity = climbVelocity;

        isClimbing = Mathf.Abs(rb2d.velocity.x) > Mathf.Epsilon || Mathf.Abs(rb2d.velocity.y) > Mathf.Epsilon;
        anim.SetBool("isClimbing", isClimbing);
    }
}
