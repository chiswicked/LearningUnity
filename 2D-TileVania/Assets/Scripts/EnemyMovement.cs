using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private float speed = 1f;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        rb.velocity = new Vector2(speed, 0f);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        speed *= -1;
        transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
    }
}
