# Learning Unity
Completed project files for  online Unity game development courses.

## 1. Beginner Unity Bundle
[Course bundle](https://www.gamedev.tv/p/beginner-unity-bundle/)

### 1.1 Complete C# Unity Game Developer 2D Online Course - 2021
[Online course](https://www.gamedev.tv/p/unity-2d-game-dev-course-2021)

#### 1.1.1 Delivery Driver [project files](./2D-DeliveryDriver)
#### 1.1.2 Snow Boarder [project files](./2D-SnowBoarder)
#### 1.1.3 Quiz Master [project files](./2D-QuizMaster)
#### 1.1.4 Tile Vania [project files](./2D-TileVania)

### 1.2 Complete C# Unity Game Developer 3D Online Course - 2020
[Online course](https://www.gamedev.tv/p/complete-c-unity-game-developer-3d-online-course-2020/)

#### 1.2.1 Obstacle Course [project files](./3D-ObstacleCourse)
~                                                                    
