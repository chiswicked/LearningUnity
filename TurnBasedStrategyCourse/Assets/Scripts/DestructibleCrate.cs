using System;
using UnityEngine;

public class DestructibleCrate : MonoBehaviour {
    [SerializeField] private Transform crateDestroyedPrefab;

    public static event EventHandler OnAnyDestroyed;

    private GridPosition gridPosition;

    private float explosionForce = 120f;
    private Vector3 explosionPosition;
    private float explosionRange = 1.5f;

    // Lifecycle Event Handlers
    // =============================================================================================

    private void Start() {
        gridPosition = LevelGrid.Instance.GetGridPosition(transform.position);
    }

    // Public Methods
    // =============================================================================================

    public GridPosition GetGridPosition() => gridPosition;

    public void Damage() {
        Transform crateDestroyedTransform =
            Instantiate(crateDestroyedPrefab, transform.position, transform.rotation);
        explosionPosition = transform.position;
        explosionPosition.x += UnityEngine.Random.Range(-0.25f, 0.25f);
        explosionPosition.y += UnityEngine.Random.Range(-0.25f, 0.25f);
        explosionPosition.z += UnityEngine.Random.Range(-0.25f, 0.25f);
        ApplyExplosionToChildren(crateDestroyedTransform, explosionForce, transform.position, explosionRange);
        Destroy(gameObject);
        OnAnyDestroyed?.Invoke(this, new EventArgs());
    }

    private void ApplyExplosionToChildren(Transform root, float explosionForce, Vector3 explosionPosition, float explosionRange) {
        foreach (Transform child in root) {
            if (child.TryGetComponent<Rigidbody>(out Rigidbody childRigidbody)) {
                childRigidbody.AddExplosionForce(explosionForce, explosionPosition, explosionRange);
            }
            ApplyExplosionToChildren(child, explosionForce, explosionPosition, explosionRange);
        }
    }
}
