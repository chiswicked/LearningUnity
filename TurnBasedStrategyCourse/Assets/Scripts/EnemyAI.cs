using System;
using Unity.VisualScripting;
using UnityEngine;

public class EnemyAI : MonoBehaviour {
    private float timer;

    private State state;
    private enum State {
        WaitingForEnemyTurn,
        TakingTurn,
        Busy,
    }

    // Lifecycle Event Handlers
    // =============================================================================================

    private void Awake() {
        state = State.WaitingForEnemyTurn;
    }

    private void Start() {
        TurnSystem.Instance.OnTurnChanged += TurnSystem_OnTurnChanged;
    }

    private void Update() {
        if (TurnSystem.Instance.IsPlayerTurn()) { return; }

        switch (state) {
            case State.WaitingForEnemyTurn:
                break;
            case State.TakingTurn:
                timer -= Time.deltaTime;
                if (timer <= 0f) {
                    if (TryExecuteAction(SetStateTakingTurn)) {
                        state = State.Busy;
                    } else {
                        TurnSystem.Instance.NextTurn();
                    }
                }
                break;
            case State.Busy:
                break;
        }
    }

    // Application Event Handlers
    // =============================================================================================

    private void TurnSystem_OnTurnChanged(object sender, EventArgs e) {
        if (!TurnSystem.Instance.IsPlayerTurn()) {
            timer = 2f;
            state = State.TakingTurn;
        }
    }

    // Private Methods
    // =============================================================================================

    private void SetStateTakingTurn() {
        timer = 0.5f;
        state = State.TakingTurn;
    }

    private bool TryExecuteAction(Action onActionComplete) {
        foreach (Unit enemyUnit in UnitManager.Instance.GetEnemyUnitList()) {
            if (TryExecuteAction(enemyUnit, onActionComplete)) { return true; }
        }
        return false;
    }

    private bool TryExecuteAction(Unit enemyUnit, Action onActionComplete) {
        if (enemyUnit == null) { return false; }

        EnemyAIAction bestEnemyAIAction = null;
        BaseAction bestBaseAction = null;
        foreach (BaseAction baseAction in enemyUnit.GetBaseActionArray()) {
            if (!enemyUnit.CanSpendActionPointsToExecuteAction(baseAction)) {
                // Enemy cannot afford this action
                continue;
            }
            if (bestEnemyAIAction == null) {
                bestEnemyAIAction = baseAction.GetBestEnemyAIAction();
                bestBaseAction = baseAction;
            } else {
                EnemyAIAction testEnemyAIAction = baseAction.GetBestEnemyAIAction();
                if (testEnemyAIAction != null &&
                    testEnemyAIAction.actionValue > bestEnemyAIAction.actionValue) {
                    bestEnemyAIAction = testEnemyAIAction;
                    bestBaseAction = baseAction;
                }
            }
        }

        if (bestEnemyAIAction == null ||
            !enemyUnit.TrySpendActionPointsToExecuteAction(bestBaseAction)) {
            return false;
        }

        bestBaseAction.ExecuteAction(bestEnemyAIAction.gridPosition, onActionComplete);
        return true;
    }
}
