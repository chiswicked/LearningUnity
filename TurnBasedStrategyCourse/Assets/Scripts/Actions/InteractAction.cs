using System;
using System.Collections.Generic;
using UnityEngine;

public class InteractAction : BaseAction {
    private int maxInteractDistance = 1;
    // Lifecycle Event Handlers
    // =============================================================================================

    private void Update() {
        if (!isActive) {
            return;
        }
    }

    // Public Methods
    // =============================================================================================

    public override void ExecuteAction(GridPosition gridPosition, Action onActionComplete) {
        IInteractable interactable = LevelGrid.Instance.GetInteractableAtGridposition(gridPosition);
        interactable.Interact(OnInteractionAnimationComplete);
        ActionStart(onActionComplete);
    }

    public override string GetActionName() => "Interact";

    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition) =>
        new EnemyAIAction {
            gridPosition = gridPosition,
            actionValue = 0,
        };

    public override List<GridPosition> GetValidActionGridPositionList() {
        List<GridPosition> validGridPositionList = new List<GridPosition>();
        GridPosition unitGridPosition = unit.GetGridPosition();

        for (int x = -maxInteractDistance; x <= maxInteractDistance; x++) {
            for (int z = -maxInteractDistance; z <= maxInteractDistance; z++) {
                GridPosition offsetGridPosition = new GridPosition(x, z);
                GridPosition testGridPosition = unitGridPosition + offsetGridPosition;

                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition)) {
                    continue;
                }

                IInteractable interactable = LevelGrid.Instance.GetInteractableAtGridposition(testGridPosition);
                if (interactable == null) { continue; }

                validGridPositionList.Add(testGridPosition);
            }
        }
        return validGridPositionList;
    }

    // Private Methods
    // =============================================================================================

    private void OnInteractionAnimationComplete() {
        ActionComplete();
    }
}
