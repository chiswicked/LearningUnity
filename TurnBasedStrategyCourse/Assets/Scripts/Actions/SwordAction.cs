using System;
using System.Collections.Generic;
using UnityEngine;

public class SwordAction : BaseAction {
    public static event EventHandler OnAnySwordHit;
    public event EventHandler OnSwordActionStarted;
    public event EventHandler OnSwordActionCompleted;

    private int maxSwordDistance = 1;

    private State state;
    private enum State {
        SwingingSwordBeforeHit,
        SwingingSwordAfterHit,
    }
    private float stateTimer;
    private float beforeHitStateTime = 0.7f;
    private float afterHitStateTime = 0.5f;

    private Unit targetUnit;

    private int damageAmount = 90;


    // Lifecycle Event Handlers
    // =============================================================================================


    private void Update() {
        if (!isActive) { return; }

        stateTimer -= Time.deltaTime;

        switch (state) {
            case State.SwingingSwordBeforeHit:
                Vector3 aimDirection = (targetUnit.GetWorldPosition() - unit.GetWorldPosition()).normalized;
                transform.forward = Vector3.Lerp(transform.forward, aimDirection, Time.deltaTime * 5f);
                break;
            case State.SwingingSwordAfterHit:
                break;
        }

        if (stateTimer <= 0f) {
            NextState();
        }
    }

    public override void ExecuteAction(GridPosition gridPosition, Action onActionComplete) {
        targetUnit = LevelGrid.Instance.GetUnitAtGridPosition(gridPosition);
        state = State.SwingingSwordBeforeHit;
        stateTimer = beforeHitStateTime;

        OnSwordActionStarted?.Invoke(this, new EventArgs());
        ActionStart(onActionComplete);
    }

    public override string GetActionName() => "Sword";

    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition) =>
        new EnemyAIAction {
            gridPosition = gridPosition,
            actionValue = 200,
        };

    public override List<GridPosition> GetValidActionGridPositionList() {
        List<GridPosition> validGridPositionList = new List<GridPosition>();
        GridPosition unitGridPosition = unit.GetGridPosition();

        for (int x = -maxSwordDistance; x <= maxSwordDistance; x++) {
            for (int z = -maxSwordDistance; z <= maxSwordDistance; z++) {
                GridPosition offsetGridPosition = new GridPosition(x, z);
                GridPosition testGridPosition = unitGridPosition + offsetGridPosition;

                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition)) {
                    continue;
                }

                // Discard cells that have no units on them other units are
                if (!LevelGrid.Instance.HasAnyUnitOnGridPosition(testGridPosition)) {
                    continue;
                }

                // Discard cells with units from the same faction
                Unit targetUnit = LevelGrid.Instance.GetUnitAtGridPosition(testGridPosition);
                if (targetUnit.IsEnemy() == unit.IsEnemy()) {
                    continue;
                }

                validGridPositionList.Add(testGridPosition);
            }
        }
        return validGridPositionList;
    }

    public int GetMaxSwordDistance() => maxSwordDistance;

    private void NextState() {
        switch (state) {
            case State.SwingingSwordBeforeHit:
                state = State.SwingingSwordAfterHit;
                stateTimer = afterHitStateTime;
                targetUnit.Damage(damageAmount);
                OnAnySwordHit?.Invoke(this, new EventArgs());
                break;
            case State.SwingingSwordAfterHit:
                OnSwordActionCompleted?.Invoke(this, new EventArgs());
                ActionComplete();
                break;
        }
    }
}
