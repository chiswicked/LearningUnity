using System;
using System.Collections.Generic;
using UnityEngine;

public class MoveAction : BaseAction {
    public event EventHandler OnStartMoving;
    public event EventHandler OnStopMoving;

    private int maxMoveDistance = 5;

    private List<Vector3> positionList;
    private int currentPositionIndex;

    private readonly float moveSpeed = 4f;
    private readonly float rotateSpeed = 14f;

    // Lifecycle Event Handlers
    // =============================================================================================

    private void Update() {
        if (!isActive) { return; }

        Vector3 targetPosition = positionList[currentPositionIndex];
        Vector3 moveDirection = (targetPosition - transform.position).normalized;

        transform.forward = Vector3.Lerp(transform.forward, moveDirection, Time.deltaTime * rotateSpeed);

        float stoppingDistance = .1f;
        if (Vector3.Distance(transform.position, targetPosition) > stoppingDistance) {
            transform.position += moveDirection * moveSpeed * Time.deltaTime;
        } else {
            currentPositionIndex++;
            if (currentPositionIndex >= positionList.Count) {
                OnStopMoving?.Invoke(this, EventArgs.Empty);
                ActionComplete();
            }
        }
    }

    // Public Methods
    // =============================================================================================

    public override void ExecuteAction(GridPosition gridPosition, Action onActionComplete) {
        List<GridPosition> pathGridPositionList =
            Pathfinding.Instance.FindPath(unit.GetGridPosition(), gridPosition, out int pathLength);

        currentPositionIndex = 0;
        positionList = new List<Vector3>();

        foreach (GridPosition pathGridPosition in pathGridPositionList) {
            positionList.Add(LevelGrid.Instance.GetWorldPosition(pathGridPosition));
        }

        OnStartMoving?.Invoke(this, EventArgs.Empty);
        ActionStart(onActionComplete);
    }

    public override List<GridPosition> GetValidActionGridPositionList() {
        List<GridPosition> validGridPositionList = new List<GridPosition>();
        GridPosition unitGridPosition = unit.GetGridPosition();

        for (int x = -maxMoveDistance; x <= maxMoveDistance; x++) {
            for (int z = -maxMoveDistance; z <= maxMoveDistance; z++) {
                GridPosition offsetGridPosition = new GridPosition(x, z);
                GridPosition testGridPosition = unitGridPosition + offsetGridPosition;

                // Discard cells outside of range
                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition)) {
                    continue;
                }
                // Discard cell where unit is
                if (unitGridPosition == testGridPosition) {
                    continue;
                }
                // Discard cells where other units are
                if (LevelGrid.Instance.HasAnyUnitOnGridPosition(testGridPosition)) {
                    continue;
                }
                // Discard cells that are unwalkable
                if (!Pathfinding.Instance.IsWalkableGridPosition(testGridPosition)) {
                    continue;
                }
                // Discard cells that are unreachable
                if (!Pathfinding.Instance.IsReachableGridPosition(unitGridPosition, testGridPosition)) {
                    continue;
                }
                // Discard cells that are not within wealking distnace
                if (Pathfinding.Instance.GetPathLength(
                    unitGridPosition, testGridPosition) > maxMoveDistance * Pathfinding.MOVE_COST_MULTIPLIER
                ) {
                    continue;
                }
                validGridPositionList.Add(testGridPosition);
            }
        }
        return validGridPositionList;
    }

    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition) {
        ShootAction shootAction = unit.GetAction<ShootAction>();
        int targetCountAtGridPosition = shootAction.GetTargetCountAtGridPosition(gridPosition);

        return new EnemyAIAction {
            gridPosition = gridPosition,
            actionValue = targetCountAtGridPosition * 10,
        };
    }

    public override string GetActionName() => "Move";

    public override int GetActionPointsCost() => 2;
}
