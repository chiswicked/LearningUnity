using System;
using System.Collections.Generic;
using UnityEngine;

public class SpinAction : BaseAction {
    private float totalSpinAMount;

    // Lifecycle Event Handlers
    // =============================================================================================

    private void Update() {
        if (!isActive) { return; }

        float spinAddAmount = 360f * Time.deltaTime;
        totalSpinAMount += spinAddAmount;
        if (totalSpinAMount >= 360f) {
            totalSpinAMount = 0f;
            ActionComplete();
        }
        transform.eulerAngles += new Vector3(0, spinAddAmount, 0);
    }

    // Public Methods
    // =============================================================================================

    public override void ExecuteAction(GridPosition _, Action onActionComplete) {
        ActionStart(onActionComplete);
    }

    public override List<GridPosition> GetValidActionGridPositionList() {
        return new List<GridPosition> { unit.GetGridPosition() };
    }

    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition) {
        return new EnemyAIAction {
            gridPosition = gridPosition,
            actionValue = 0,
        };
    }

    public override string GetActionName() => "Spin";
}
