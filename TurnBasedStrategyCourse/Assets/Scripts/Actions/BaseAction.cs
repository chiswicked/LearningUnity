using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseAction : MonoBehaviour {
    public static EventHandler OnAnyActionStarted;
    public static EventHandler OnAnyActionCompleted;

    protected bool isActive;
    protected Unit unit;
    protected Action onActionComplete;

    // Lyfecycle Event Handlers
    // =============================================================================================

    protected virtual void Awake() {
        unit = GetComponent<Unit>();
    }

    // Public Methods
    // =============================================================================================

    public abstract string GetActionName();

    public Unit GetUnit() => unit;

    public abstract void ExecuteAction(GridPosition gridPosition, Action onActionComplete);

    public abstract List<GridPosition> GetValidActionGridPositionList();

    public virtual bool IsValidActionGridPosition(GridPosition gridPosition) =>
        GetValidActionGridPositionList().Contains(gridPosition);

    public abstract EnemyAIAction GetEnemyAIAction(GridPosition gridPosition);

    public EnemyAIAction GetBestEnemyAIAction() {
        List<EnemyAIAction> enemyAIActionList = new List<EnemyAIAction>();
        List<GridPosition> validActionGridPositionList = GetValidActionGridPositionList();

        foreach (GridPosition gridPosition in validActionGridPositionList) {
            EnemyAIAction enemyAIAction = GetEnemyAIAction(gridPosition);
            enemyAIActionList.Add(enemyAIAction);
        }
        if (enemyAIActionList.Count == 0) { return null; }

        enemyAIActionList.Sort((EnemyAIAction a, EnemyAIAction b) => b.actionValue - a.actionValue);
        return enemyAIActionList[0];
    }

    public virtual int GetActionPointsCost() => 1;

    // Protected Methods
    // =============================================================================================

    protected void ActionStart(Action onActionComplete) {
        isActive = true;
        this.onActionComplete = onActionComplete;
        OnAnyActionStarted?.Invoke(this, new EventArgs());
    }

    protected void ActionComplete() {
        isActive = false;
        onActionComplete();
        OnAnyActionCompleted?.Invoke(this, new EventArgs());
    }
}
