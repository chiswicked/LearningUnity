using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ActionButtonUI : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI textMeshPro;
    [SerializeField] private Button button;
    [SerializeField] private GameObject selectedVisual;

    private BaseAction baseAction;

    public void SetBaseAction(BaseAction baseAction) {
        this.baseAction = baseAction;
        textMeshPro.text = baseAction.GetActionName().ToUpper();
        button.onClick.AddListener(() => {
            UnitActionSystem.Instance.SetSelectedAction(baseAction);
        });
    }

    public void UpdateSelectedVisual() {
        BaseAction selectedBaseAction = UnitActionSystem.Instance.GetSelectedAction();
        bool hide = selectedBaseAction == null || baseAction != selectedBaseAction;
        selectedVisual.SetActive(!hide);
    }
}
