using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TurnSystemUI : MonoBehaviour {
    [SerializeField] private Button endTurnButton;
    [SerializeField] private TextMeshProUGUI currentTurnText;
    [SerializeField] private GameObject enemyTurnVisual;

    private void Start() {
        UpdateCurrentTurnText();
        UpdateEnemyTurnVisual();
        UpdateEndTurnButtonVisibility();

        endTurnButton.onClick.AddListener(() => {
            TurnSystem.Instance.NextTurn();
        });
        TurnSystem.Instance.OnTurnChanged += TurnSystemUI_OnTurnChanged;
    }

    private void TurnSystemUI_OnTurnChanged(object sender, EventArgs e) {
        UpdateCurrentTurnText();
        UpdateEnemyTurnVisual();
        UpdateEndTurnButtonVisibility();
    }

    private void UpdateCurrentTurnText() {
        currentTurnText.text = $"TURN {TurnSystem.Instance.GetCurrentTurn()}";
    }

    private void UpdateEnemyTurnVisual() {
        enemyTurnVisual.SetActive(TurnSystem.Instance.IsEnemyTurn());
    }

    private void UpdateEndTurnButtonVisibility() {
        endTurnButton.gameObject.SetActive(TurnSystem.Instance.IsPlayerTurn());
    }
}
