using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class HealthSystem : MonoBehaviour {
    public event EventHandler OnDead;
    public event EventHandler OnChangeHealth;

    private int currentHealth = 100;
    private int maxHealth = 100;

    public void TakeDamage(int damage) {
        if (currentHealth <= damage) {
            currentHealth = 0;
            Die();
            return;
        }

        currentHealth -= damage;
        OnChangeHealth?.Invoke(this, new EventArgs());
    }

    public float GetHealthNormalized() {
        return (float)currentHealth / maxHealth;
    }

    private void Die() {
        OnDead?.Invoke(this, new EventArgs());
    }
}
