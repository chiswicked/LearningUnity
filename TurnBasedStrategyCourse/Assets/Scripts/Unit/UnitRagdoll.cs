using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class UnitRagdoll : MonoBehaviour {
    [SerializeField] private Transform ragdollRootBone;

    private float explosionForce = 500f;
    private Vector3 explosionPosition;
    private float explosionRange = 10f;

    public void Setup(Transform originalRagdollRootBone) {
        MatchAllChildTransforms(originalRagdollRootBone, ragdollRootBone);

        explosionPosition = transform.position;
        explosionPosition.y = 1f + Random.Range(-0.5f, 0.5f);

        ApplyExplosionToRagdoll(ragdollRootBone, explosionForce, explosionPosition, explosionRange);
    }

    private void MatchAllChildTransforms(Transform root, Transform clone) {
        foreach (Transform child in root) {
            Transform cloneChild = clone.Find(child.name);
            if (cloneChild != null) {
                cloneChild.position = child.position;
                cloneChild.rotation = child.rotation;
                MatchAllChildTransforms(child, cloneChild);
            }
        }
    }

    private void ApplyExplosionToRagdoll(Transform root, float explosionForce, Vector3 explosionPosition, float explosionRange) {
        foreach (Transform child in root) {
            if (child.TryGetComponent<Rigidbody>(out Rigidbody childRigidbody)) {
                childRigidbody.AddExplosionForce(explosionForce, explosionPosition, explosionRange);
            }
            ApplyExplosionToRagdoll(child, explosionForce, explosionPosition, explosionRange);
        }
    }
}
