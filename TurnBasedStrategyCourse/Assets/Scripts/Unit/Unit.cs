using System;
using UnityEngine;

public class Unit : MonoBehaviour {
    [SerializeField] private bool isEnemy;
    public static event EventHandler OnAnyActionPointsChanged;
    public static event EventHandler OnAnyUnitSpawned;
    public static event EventHandler OnAnyUnitDead;

    private GridPosition gridPosition;

    private HealthSystem healthSystem;

    private BaseAction[] baseActionArray;

    private const int DEFAULT_ACTION_POINTS = 5;
    private int actionPoints;
    private readonly float shoulderHeight = 1.7f;

    // Lifecycle Event Handlers
    // =============================================================================================

    public void Awake() {
        healthSystem = GetComponent<HealthSystem>();
        baseActionArray = GetComponents<BaseAction>();

        ResetActionPoints();
    }

    private void Start() {
        this.gridPosition = LevelGrid.Instance.GetGridPosition(transform.position);
        LevelGrid.Instance.SetUnitAtGridposition(gridPosition, this);

        TurnSystem.Instance.OnTurnChanged += TurnSystem_OnTurnChanged;
        healthSystem.OnDead += HealthSystem_OnDead;

        OnAnyUnitSpawned?.Invoke(this, new EventArgs());
    }

    private void Update() {
        GridPosition newGridPosition = LevelGrid.Instance.GetGridPosition(transform.position);
        if (newGridPosition != gridPosition) {
            GridPosition oldGridPosition = gridPosition;
            gridPosition = newGridPosition;

            LevelGrid.Instance.UnitMovedGridPosition(this, oldGridPosition, newGridPosition);
        }
    }

    // Application Event Handlers
    // =============================================================================================

    private void TurnSystem_OnTurnChanged(object sender, EventArgs e) {
        if (isEnemy != TurnSystem.Instance.IsEnemyTurn()) { return; }

        ResetActionPoints();
    }

    private void HealthSystem_OnDead(object sender, EventArgs e) {
        LevelGrid.Instance.RemoveUnitAtGridPosition(gridPosition, this);
        Destroy(gameObject);
        OnAnyUnitDead?.Invoke(this, new EventArgs());
    }

    // Public Methods
    // =============================================================================================

    public GridPosition GetGridPosition() => gridPosition;

    public Vector3 GetWorldPosition() => transform.position;

    public T GetAction<T>() where T : BaseAction {
        foreach (BaseAction baseAction in baseActionArray) {
            if (baseAction is T) {
                return baseAction as T;
            }
        }
        return null;
    }

    public BaseAction[] GetBaseActionArray() {
        return baseActionArray;
    }

    public bool TrySpendActionPointsToExecuteAction(BaseAction baseAction) {
        if (!CanSpendActionPointsToExecuteAction(baseAction)) {
            return false;
        }
        SpendActionPoints(baseAction.GetActionPointsCost());
        return true;

    }

    public bool CanSpendActionPointsToExecuteAction(BaseAction baseAction) =>
        (actionPoints >= baseAction.GetActionPointsCost());

    public int GetActionPointsLeft() => actionPoints;

    public bool IsEnemy() => isEnemy;

    public void Damage(int damage) {
        healthSystem.TakeDamage(damage);
    }

    public float GetHealthNormalized() {
        return healthSystem.GetHealthNormalized();
    }

    public float GetShoulderHeight() => shoulderHeight;

    public override string ToString() => $"{base.ToString()}";

    // Private Methods
    // =============================================================================================

    private void ResetActionPoints() {
        actionPoints = Unit.DEFAULT_ACTION_POINTS;
        OnAnyActionPointsChanged?.Invoke(this, new EventArgs());
    }

    private void SpendActionPoints(int amount) {
        actionPoints -= amount;
        OnAnyActionPointsChanged?.Invoke(this, new EventArgs());
    }

}
