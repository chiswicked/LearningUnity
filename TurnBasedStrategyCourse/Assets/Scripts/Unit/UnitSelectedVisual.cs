using System;
using UnityEngine;

public class UnitSelectedVisual : MonoBehaviour {
    [SerializeField] private Unit unit;

    private MeshRenderer meshRenderer;

    private void Awake() {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    private void Start() {
        UnitActionSystem.Instance.OnSelectedUnitChange += UnitActionSystem_OnSelectedUnitChange;
        UpdateVisual();
    }

    private void UnitActionSystem_OnSelectedUnitChange(object sender, EventArgs empty) {
        UpdateVisual();
    }

    private void UpdateVisual() {
        if (UnitActionSystem.Instance.GetSelectedUnit() == null || UnitActionSystem.Instance.GetSelectedUnit() != unit) {
            meshRenderer.enabled = false;
        } else {
            meshRenderer.enabled = true;
        }
    }

    private void OnDestroy() {
        UnitActionSystem.Instance.OnSelectedUnitChange -= UnitActionSystem_OnSelectedUnitChange;
    }
}
