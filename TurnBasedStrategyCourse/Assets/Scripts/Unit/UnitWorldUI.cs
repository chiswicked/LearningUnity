using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UnitWorldUI : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI actionPointsText;
    [SerializeField] private Unit unit;
    [SerializeField] private Image healthBarImage;
    [SerializeField] private HealthSystem healthSystem;

    private static Color HEALTHY_COLOR = new Color(0f, 1f, 0f, 1f);
    private static Color BRUISED_COLOR = new Color(1f, 1f, 0f, 1f);
    private static Color DAMAGED_COLOR = new Color(1f, 0.5f, 0f, 1f);
    private static Color SERIOUS_COLOR = new Color(1f, 0f, 0f, 1f);

    private void Start() {
        Unit.OnAnyActionPointsChanged += Unit_OnAnyActionPointsChanged;
        healthSystem.OnChangeHealth += HealthSystem_OnChangeHealth;

        UpdateActionPointsText();
        UpdateHealthBar();
    }

    private void HealthSystem_OnChangeHealth(object sender, EventArgs e) {
        UpdateHealthBar();
    }

    private void Unit_OnAnyActionPointsChanged(object sender, EventArgs e) {
        UpdateActionPointsText();
    }

    private void UpdateHealthBar() {
        float healthPercentage = healthSystem.GetHealthNormalized();
        healthBarImage.fillAmount = healthPercentage;
        switch (healthPercentage) {
            case > 0.75f:
                healthBarImage.color = UnitWorldUI.HEALTHY_COLOR;
                break;
            case > 0.5f:
                healthBarImage.color = UnitWorldUI.BRUISED_COLOR;
                break;
            case > 0.25f:
                healthBarImage.color = UnitWorldUI.DAMAGED_COLOR;
                break;
            default:
                healthBarImage.color = UnitWorldUI.SERIOUS_COLOR;
                break;
        }

    }

    private void UpdateActionPointsText() {
        actionPointsText.text = unit.GetActionPointsLeft().ToString();
    }

    private void OnDestroy() {
        Unit.OnAnyActionPointsChanged -= Unit_OnAnyActionPointsChanged;
    }
}
