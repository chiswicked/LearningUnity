using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnitActionSystem : MonoBehaviour {
    public static UnitActionSystem Instance { get; private set; }

    [SerializeField] private LayerMask unitsLayerMask;
    [SerializeField] private Unit selectedUnit;
    [SerializeField] private BaseAction selectedAction;

    private bool isBusy;

    public event EventHandler OnSelectedUnitChange;
    public event EventHandler OnSelectedActionChange;
    public event EventHandler<bool> OnBusyChanged;
    public event EventHandler OnActionStarted;

    private void Awake() {
        if (Instance != null) {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    private void Update() {
        if (isBusy) { return; }
        if (TurnSystem.Instance.IsEnemyTurn()) { return; }
        if (EventSystem.current.IsPointerOverGameObject()) { return; }

        if (InputManager.Instance.IsMouseButtonLeftPressedThisFrame()) {
            if (TryHandleUnitSelection()) return;
            HandleSelectedAction();
        }
    }

    private bool TryHandleUnitSelection() {
        Ray ray = Camera.main.ScreenPointToRay(InputManager.Instance.GetMouseScreenPosition());
        if (Physics.Raycast(ray, out RaycastHit raycastHit, float.MaxValue, unitsLayerMask)) {
            if (raycastHit.transform.TryGetComponent<Unit>(out Unit unit)) {
                if (unit == selectedUnit || unit.IsEnemy()) { return false; }
                SetSelectedUnit(unit);
                return true;
            }
        }
        return false;
    }

    private void HandleSelectedAction() {
        if (selectedUnit == null || selectedAction == null) { return; }
        GridPosition mouseGridPosition = LevelGrid.Instance.GetGridPosition(MouseWorld.GetPosition());

        if (!selectedAction.IsValidActionGridPosition(mouseGridPosition)) { return; }
        if (!selectedUnit.TrySpendActionPointsToExecuteAction(selectedAction)) { return; }
        SetBusy();
        selectedAction.ExecuteAction(mouseGridPosition, ClearBusy);
        OnActionStarted?.Invoke(this, EventArgs.Empty);
    }

    private void SetSelectedUnit(Unit unit) {
        selectedUnit = unit;
        OnSelectedUnitChange?.Invoke(this, EventArgs.Empty);
        SetSelectedAction(selectedUnit.GetAction<MoveAction>());
    }

    public BaseAction GetSelectedAction() {
        return selectedAction;
    }

    public void SetSelectedAction(BaseAction baseAction) {
        selectedAction = baseAction;
        OnSelectedActionChange?.Invoke(this, EventArgs.Empty);
    }

    public Unit GetSelectedUnit() {
        return selectedUnit;
    }

    private void SetBusy() {
        isBusy = true;
        OnBusyChanged?.Invoke(this, isBusy);
    }

    private void ClearBusy() {
        isBusy = false;
        OnBusyChanged?.Invoke(this, isBusy);
    }
}
