using System;
using System.Collections.Generic;
using UnityEngine;

public class UnitManager : MonoBehaviour {
    public static UnitManager Instance { get; private set; }

    private List<Unit> unitList = new List<Unit>();
    private List<Unit> friendlyUnitList = new List<Unit>();
    private List<Unit> enemyUnitList = new List<Unit>();

    // Lifecycle Event Handlers
    // =============================================================================================

    private void Awake() {
        if (Instance != null) {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    private void Start() {
        Unit.OnAnyUnitSpawned += Unit_OnAnyUnitSpawned;
        Unit.OnAnyUnitDead += Unit_OnAnyUnitDead;
    }

    // Application Event Handlers
    // =============================================================================================

    private void Unit_OnAnyUnitSpawned(object sender, EventArgs e) {
        Unit unit = sender as Unit;
        if (unit == null) { return; }

        unitList.Add(unit);
        if (unit.IsEnemy()) {
            enemyUnitList.Add(unit);
        } else {
            friendlyUnitList.Add(unit);
        }
    }

    private void Unit_OnAnyUnitDead(object sender, EventArgs e) {
        Unit unit = sender as Unit;
        if (unit == null) { return; }

        unitList.Remove(unit);
        if (unit.IsEnemy()) {
            enemyUnitList.Remove(unit);
        } else {
            friendlyUnitList.Remove(unit);
        }
    }

    // Public Methods
    // =============================================================================================

    public List<Unit> GetUnitList() => unitList;

    public List<Unit> GetFriendlyUnitList() => friendlyUnitList;

    public List<Unit> GetEnemyUnitList() => enemyUnitList;

}
