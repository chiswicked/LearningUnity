using System;
using UnityEngine;

public class PathfindingUpdater : MonoBehaviour {

    // Lifecycle Event Handlers
    // =============================================================================================

    private void Start() {
        DestructibleCrate.OnAnyDestroyed += DestructibleCrate_OnAnyDestroyed;
    }

    // Private Methods
    // =============================================================================================

    private void DestructibleCrate_OnAnyDestroyed(object sender, EventArgs e) {
        DestructibleCrate destructibleCrate = sender as DestructibleCrate;
        Pathfinding.Instance.SetIsWalkableGridPosition(destructibleCrate.GetGridPosition(), true);
    }
}
