using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : MonoBehaviour {
    [SerializeField] private TrailRenderer trailRenderer;
    [SerializeField] private Transform bulletHitVFXPrefab;

    private Vector3 targetPosition;
    private float moveSpeed = 100f;
    public void Setup(Vector3 targetPosition) {
        this.targetPosition = targetPosition;
    }

    public void Update() {
        Vector3 moveDirection = (targetPosition - transform.position).normalized;
        float distanceBefore = Vector3.Distance(transform.position, targetPosition);
        transform.position += moveDirection * moveSpeed * Time.deltaTime;
        float distanceAfter = Vector3.Distance(transform.position, targetPosition);

        if (distanceBefore < distanceAfter) {
            transform.position = targetPosition;
            trailRenderer.transform.parent = null;
            Destroy(gameObject);
            Instantiate(bulletHitVFXPrefab, transform.position, Quaternion.identity);
        }
    }
}
