using TMPro;
using UnityEngine;

public class GridDebugObject : MonoBehaviour {
    [SerializeField] private TextMeshPro coordinatesText;

    private object gridObject;

    // Lifecycle Event Handlers
    // =============================================================================================

    protected virtual void Update() {
        coordinatesText.text = gridObject.ToString();
    }

    // Public Methods
    // =============================================================================================

    public virtual void SetGridObject(object gridObject) =>
        this.gridObject = gridObject;
}