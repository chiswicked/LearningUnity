using System;

public struct GridPosition : IEquatable<GridPosition> {
    public int x;
    public int z;

    public static readonly GridPosition UP = new(0, 1);
    public static readonly GridPosition DOWN = new(0, -1);
    public static readonly GridPosition RIGHT = new(1, 0);
    public static readonly GridPosition LEFT = new(-1, 0);

    // Constructor
    // =============================================================================================

    public GridPosition(int x, int z) {
        this.x = x;
        this.z = z;
    }

    // Public Methods
    // =============================================================================================

    public bool Equals(GridPosition other) => this == other;

    public static GridPosition operator +(GridPosition lhs, GridPosition rhs) =>
        new GridPosition(lhs.x + rhs.x, lhs.z + rhs.z);

    public static GridPosition operator -(GridPosition lhs, GridPosition rhs) =>
        new GridPosition(lhs.x - rhs.x, lhs.z - rhs.z);

    public static bool operator ==(GridPosition lhs, GridPosition rhs) =>
        lhs.x == rhs.x && lhs.z == rhs.z;

    public static bool operator !=(GridPosition lhs, GridPosition rhs) =>
        !(lhs == rhs);

    public override bool Equals(object obj) =>
        obj is GridPosition position &&
        x == position.x &&
        z == position.z;

    public override int GetHashCode() => HashCode.Combine(x, z);

    public override string ToString() => $"x: {x}; z: {z}";
}