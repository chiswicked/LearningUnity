using System;
using System.Collections.Generic;
using UnityEngine;

public class GridSystemVisual : MonoBehaviour {
    [SerializeField] private Transform gridSystemVisualSinglePrefab;
    [SerializeField] private List<GridVisualTypeMaterial> gridVisualTypeMaterialList;

    [Serializable]
    public struct GridVisualTypeMaterial {
        public GridVisualType gridVisualType;
        public Material material;
    }

    public enum GridVisualType {
        Blue,
        Red,
        RedSoft,
        White,
        Yellow,
    }

    private GridSystemVisualSingle[,] gridSystemVisuals;

    // Lifecycle Event Handlers
    // =============================================================================================

    private void Start() {
        gridSystemVisuals = new GridSystemVisualSingle[LevelGrid.Instance.GetWidth(), LevelGrid.Instance.GetWidth()];

        for (int x = 0; x < LevelGrid.Instance.GetWidth(); x++) {
            for (int z = 0; z < LevelGrid.Instance.GetWidth(); z++) {
                Vector3 gridPosition = LevelGrid.Instance.GetWorldPosition(new GridPosition(x, z));
                Transform gridSystemVisualSingleTransform = Instantiate(gridSystemVisualSinglePrefab, gridPosition, Quaternion.identity);
                gridSystemVisuals[x, z] = gridSystemVisualSingleTransform.GetComponent<GridSystemVisualSingle>();
            }
        }
        UnitActionSystem.Instance.OnSelectedActionChange += UnitActionSystem_OnSelectedActionChange;
        LevelGrid.Instance.OnAnyUnitMovedGridPosition += LevelGrid_OnAnyUnitMovedGridPosition;

        UpdateGridVisual();
    }

    // Application Event Handlers
    // =============================================================================================

    private void UnitActionSystem_OnSelectedActionChange(object sender, EventArgs e) {
        UpdateGridVisual();
    }

    private void LevelGrid_OnAnyUnitMovedGridPosition(object sender, EventArgs e) {
        UpdateGridVisual();
    }

    // Public Methods
    // =============================================================================================

    public void ShowGridPositionVisualList(List<GridPosition> gridPositionList, GridVisualType gridVisualType) {
        foreach (GridPosition gridPosition in gridPositionList) {
            gridSystemVisuals[gridPosition.x, gridPosition.z].
                Show(GetGridVisualTypeMaterial(gridVisualType));
        }
    }

    public void HideAllGridPositionVisuals() {
        for (int x = 0; x < LevelGrid.Instance.GetWidth(); x++) {
            for (int z = 0; z < LevelGrid.Instance.GetWidth(); z++) {
                gridSystemVisuals[x, z].Hide();
            }
        }
    }

    // Private Methods
    // =============================================================================================

    private void UpdateGridVisual() {
        HideAllGridPositionVisuals();

        Unit selectedUnit = UnitActionSystem.Instance.GetSelectedUnit();
        BaseAction selectedAction = UnitActionSystem.Instance.GetSelectedAction();

        if (selectedUnit == null || selectedAction == null) { return; }

        GridVisualType gridVisualType;
        switch (selectedAction) {
            default:
            case MoveAction:
                gridVisualType = GridVisualType.White;
                break;
            case SpinAction:
                gridVisualType = GridVisualType.Blue;
                break;
            case ShootAction shootAction:
                gridVisualType = GridVisualType.Red;
                ShowGridPosiotionRange(
                    selectedUnit.GetGridPosition(),
                    shootAction.GetMaxShootDistance(),
                    GridVisualType.RedSoft
                );
                break;
            case GrenadeAction:
                gridVisualType = GridVisualType.Yellow;
                break;
            case SwordAction swordAction:
                gridVisualType = GridVisualType.Red;
                ShowGridPosiotionRangeSquare(
                    selectedUnit.GetGridPosition(),
                    swordAction.GetMaxSwordDistance(),
                    GridVisualType.RedSoft
                );
                break;
            case InteractAction:
                gridVisualType = GridVisualType.Blue;
                break;
        }

        ShowGridPositionVisualList(selectedAction.GetValidActionGridPositionList(), gridVisualType);
    }

    private void ShowGridPosiotionRange(GridPosition gridPosition, int range, GridVisualType gridVisualType) {
        List<GridPosition> gridPositionList = new List<GridPosition>();

        for (int x = -range; x <= range; x++) {
            for (int z = -range; z <= range; z++) {
                GridPosition testGridPosition = gridPosition + new GridPosition(x, z);

                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition)) { continue; }
                if ((Mathf.Abs(x) + Mathf.Abs(z)) > range) { continue; }

                gridPositionList.Add(testGridPosition);
            }
        }
        ShowGridPositionVisualList(gridPositionList, gridVisualType);
    }

    private void ShowGridPosiotionRangeSquare(GridPosition gridPosition, int range, GridVisualType gridVisualType) {
        List<GridPosition> gridPositionList = new List<GridPosition>();

        for (int x = -range; x <= range; x++) {
            for (int z = -range; z <= range; z++) {
                GridPosition testGridPosition = gridPosition + new GridPosition(x, z);

                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition)) { continue; }

                gridPositionList.Add(testGridPosition);
            }
        }
        ShowGridPositionVisualList(gridPositionList, gridVisualType);
    }

    private Material GetGridVisualTypeMaterial(GridVisualType gridVisualType) {
        foreach (GridVisualTypeMaterial gridVisualTypeMaterial in gridVisualTypeMaterialList) {
            if (gridVisualTypeMaterial.gridVisualType == gridVisualType) {
                return gridVisualTypeMaterial.material;
            }
        }
        Debug.LogError($"Could not find GridVisualTypeMaterial for GridVisualType {gridVisualType}");
        return null;
    }
}
