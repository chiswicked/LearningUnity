using System.Collections.Generic;

public class GridObject {
    private GridSystem<GridObject> gridSystem;
    private GridPosition gridPosition;
    private List<Unit> unitList;
    private IInteractable interactable;

    // Constructor
    // =============================================================================================

    public GridObject(GridSystem<GridObject> gridSystem, GridPosition gridPosition) {
        this.gridSystem = gridSystem;
        this.gridPosition = gridPosition;
        unitList = new List<Unit>();
    }

    // Public Methods
    // =============================================================================================

    public void AddUnit(Unit unit) => unitList.Add(unit);

    public void RemoveUnit(Unit unit) => unitList.Remove(unit);

    public Unit GetUnit() => HasAnyUnit() ? unitList[0] : null;

    public List<Unit> GetUnitList() => unitList;

    public bool HasAnyUnit() => unitList.Count > 0;

    public IInteractable GetInteractable() => interactable;

    public void SetInteractable(IInteractable interactable) => this.interactable = interactable;

    public override string ToString() {
        string units = "";
        foreach (Unit unit in unitList) {
            units += $"\n{unit}";
        }
        return $"{gridPosition} \n {units}";
    }
}