using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelGrid : MonoBehaviour {

    public static LevelGrid Instance { get; private set; }

    public event EventHandler OnAnyUnitMovedGridPosition;

    [SerializeField] private int width = 10;
    [SerializeField] private int height = 10;
    [SerializeField] private float cellSize = 2f;

    [SerializeField] private Transform gridDebugObjectPrefab;

    private GridSystem<GridObject> gridSystem;


    // Lifecycle Event Handlers
    // =============================================================================================

    private void Awake() {
        if (Instance != null) {
            Destroy(gameObject);
            return;
        }
        Instance = this;

        gridSystem = new GridSystem<GridObject>(width, height, cellSize,
            (GridSystem<GridObject> g, GridPosition gridPosition) => new GridObject(g, gridPosition));
        //gridSystem.CreateDebugObjects(gridDebugObjectPrefab);
    }

    private void Start() {
        Pathfinding.Instance.Setup(width, height, cellSize);
    }

    // Public Methods
    // =============================================================================================

    public void SetUnitAtGridposition(GridPosition gridPosition, Unit unit) =>
        gridSystem.GetGridObject(gridPosition).AddUnit(unit);

    public List<Unit> GetUnitListAtGridPosition(GridPosition gridPosition) =>
        gridSystem.GetGridObject(gridPosition).GetUnitList();

    public void ClearUnitAtGridPosition(GridPosition gridPosition, Unit unit) =>
        gridSystem.GetGridObject(gridPosition).RemoveUnit(unit);

    public void UnitMovedGridPosition(Unit unit, GridPosition fromGridPosition, GridPosition toGridPosition) {
        ClearUnitAtGridPosition(fromGridPosition, unit);
        SetUnitAtGridposition(toGridPosition, unit);
        OnAnyUnitMovedGridPosition?.Invoke(this, new EventArgs());
    }

    public Vector3 GetWorldPosition(GridPosition gridPosition) =>
        gridSystem.GetWorldPosition(gridPosition);

    public GridPosition GetGridPosition(Vector3 worldPosition) =>
        gridSystem.GetGridPosition(worldPosition);

    public bool IsValidGridPosition(GridPosition gridPosition) =>
        gridSystem.IsValidGridPosition(gridPosition);

    public bool HasAnyUnitOnGridPosition(GridPosition gridPosition) =>
        gridSystem.GetGridObject(gridPosition).HasAnyUnit();

    public Unit GetUnitAtGridPosition(GridPosition gridPosition) =>
        gridSystem.GetGridObject(gridPosition).GetUnit();

    public void RemoveUnitAtGridPosition(GridPosition gridPosition, Unit unit) {
        GridObject gridObject = gridSystem.GetGridObject(gridPosition);
        gridObject.RemoveUnit(unit);
    }

    public int GetWidth() => gridSystem.GetWidth();

    public int GetHeight() => gridSystem.GetHeight();

    public IInteractable GetInteractableAtGridposition(GridPosition gridPosition) =>
        gridSystem.GetGridObject(gridPosition).GetInteractable();

    public void SetInteractableAtGridposition(GridPosition gridPosition, IInteractable interactable) =>
        gridSystem.GetGridObject(gridPosition).SetInteractable(interactable);
}
