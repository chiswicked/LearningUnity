#define USE_NEW_INPUT_SYSTEM
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour {
    private PlayerInputActions playerInputActions;

    // Lyfecycle Event Handlers
    // =============================================================================================

    public static InputManager Instance { get; private set; }

    private void Awake() {
        if (Instance != null) {
            Destroy(gameObject);
            return;
        }
        Instance = this;

        playerInputActions = new PlayerInputActions();
        playerInputActions.Enable();
    }

    // Public Methods
    // =============================================================================================

    public Vector2 GetMouseScreenPosition() {
#if USE_NEW_INPUT_SYSTEM
        return Mouse.current.position.ReadValue();
#else
        return Input.mousePosition;
#endif
    }

    public bool IsMouseButtonLeftPressedThisFrame() {
#if USE_NEW_INPUT_SYSTEM
        return playerInputActions.Player.Click.WasPressedThisFrame();
#else
        return Input.GetMouseButtonDown(0);
#endif
    }

    public Vector2 GetCameraMoveVector() {
#if USE_NEW_INPUT_SYSTEM
        return playerInputActions.Player.CameraMovement.ReadValue<Vector2>();
#else
        Vector2 inputMoveDirection = new Vector2(0, 0);
        if (Input.GetKey(KeyCode.W)) {
            inputMoveDirection.y = +1f;
        }
        if (Input.GetKey(KeyCode.A)) {
            inputMoveDirection.x = -1f;
        }
        if (Input.GetKey(KeyCode.S)) {
            inputMoveDirection.y = -1f;
        }
        if (Input.GetKey(KeyCode.D)) {
            inputMoveDirection.x = +1f;
        }
        return inputMoveDirection;
#endif
    }

    internal float GetCameraRotateAmount() {
#if USE_NEW_INPUT_SYSTEM
        return playerInputActions.Player.CameraRotation.ReadValue<float>();
#else
        if (Input.GetKey(KeyCode.Q)) {
            return +1f;
        }
        if (Input.GetKey(KeyCode.E)) {
            return -1f;
        }
        return 0f;
#endif
    }

    internal float GetCameraZoomAmount() {
#if USE_NEW_INPUT_SYSTEM
        return playerInputActions.Player.CameraZoom.ReadValue<float>();
#else
        if (Input.mouseScrollDelta.y < 0) {
            return +1f;
        }
        if (Input.mouseScrollDelta.y > 0) {
            return -1f;
        }
        return 0f;
#endif
    }
}
