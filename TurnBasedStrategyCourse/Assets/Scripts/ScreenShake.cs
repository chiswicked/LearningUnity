using Cinemachine;
using System;
using UnityEngine;

public class ScreenShake : MonoBehaviour {
    private CinemachineImpulseSource cinemachineImpulseSource;

    // Lyfecycle Event Handlers
    // =============================================================================================

    private void Awake() {
        cinemachineImpulseSource = GetComponent<CinemachineImpulseSource>();
    }

    private void Start() {
        ShootAction.OnAnyShoot += ShootAction_OnAnyShoot;
        GrenadeProjectile.OnAnyGrenadeExploded += GrenadeProjectile_OnAnyGrenadeExploded;
        SwordAction.OnAnySwordHit += SwordAction_OnAnySwordHit;
    }

    // Application Event Handlers
    // =============================================================================================

    private void ShootAction_OnAnyShoot(object sender, ShootAction.OnShootEventArgs e) {
        Shake(1f);
    }

    private void GrenadeProjectile_OnAnyGrenadeExploded(object sender, EventArgs e) {
        Shake(5f);
    }

    private void SwordAction_OnAnySwordHit(object sender, EventArgs e) {
        Shake(0.5f);
    }

    // Public Methods
    // =============================================================================================

    public void Shake(float intensity = 1f) {
        cinemachineImpulseSource.GenerateImpulse(intensity);
    }
}
