using System;
using UnityEngine;

public class Door : MonoBehaviour, IInteractable {
    [SerializeField] private bool isOpen;

    private bool isActive;

    private GridPosition gridPosition;
    private Animator animator;

    private Action onInteractionComplete;
    private float timer;

    // Lyfecycle Event Handlers
    // =============================================================================================

    private void Awake() {
        animator = GetComponent<Animator>();
    }

    private void Start() {
        gridPosition = LevelGrid.Instance.GetGridPosition(transform.position);
        LevelGrid.Instance.SetInteractableAtGridposition(gridPosition, this);

        if (isOpen) {
            Open();
        } else {
            Close();
        };
    }

    private void Update() {
        if (!isActive) { return; }

        timer -= Time.deltaTime;
        if (timer <= 0f) {
            isActive = false;
            onInteractionComplete();
        }
    }


    // Public Methods
    // =============================================================================================

    public void Interact(Action onInteractionComplete) {
        this.onInteractionComplete = onInteractionComplete;

        isActive = true;
        timer = .5f;

        if (isOpen) {
            Close();
        } else {
            Open();
        };
    }

    // Private Methods
    // =============================================================================================

    private void Open() {
        isOpen = true;
        animator.SetBool("IsOpen", true);
        Pathfinding.Instance.SetIsWalkableGridPosition(gridPosition, true);
    }

    private void Close() {
        isOpen = false;
        animator.SetBool("IsOpen", false);
        Pathfinding.Instance.SetIsWalkableGridPosition(gridPosition, false);
    }
}
