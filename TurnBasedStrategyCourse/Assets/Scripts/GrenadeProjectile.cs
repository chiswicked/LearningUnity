using System;
using UnityEngine;

public class GrenadeProjectile : MonoBehaviour {
    [SerializeField] private Transform grenadeExplodeVFXPrefab;
    [SerializeField] private TrailRenderer trailRenderer;
    private AnimationCurve arcYAnimationCurve;

    public static event EventHandler OnAnyGrenadeExploded;

    private Vector3 targetPosition;
    private float reachedTargetDistance = .2f;

    private int damageAmount = 30;
    private float damageRadius = 4f;

    private float moveSpeed = 15f;
    private float totalDistance;
    private Vector3 positionXZ;
    private float throwHeight = 1.7f;

    private Action onGrenadeBehaviourComplete;

    // Lifecycle Event Handlers
    // =============================================================================================

    private void Update() {
        Vector3 moveDir = (targetPosition - positionXZ).normalized;
        positionXZ += moveDir * moveSpeed * Time.deltaTime;
        float currentDistance = Vector3.Distance(positionXZ, targetPosition);
        float distanceNormalized = 1 - currentDistance / totalDistance;
        float positionY = arcYAnimationCurve.Evaluate(distanceNormalized) * throwHeight;
        transform.position = new Vector3(positionXZ.x, positionY, positionXZ.z);

        if (Vector3.Distance(positionXZ, targetPosition) < reachedTargetDistance) {
            Collider[] colliderArray = Physics.OverlapSphere(targetPosition, damageRadius);

            foreach (Collider collider in colliderArray) {
                if (collider.TryGetComponent<Unit>(out Unit targetUnit)) {
                    targetUnit.Damage(damageAmount);
                }
                if (collider.TryGetComponent<DestructibleCrate>(out DestructibleCrate destructibleCrate)) {
                    destructibleCrate.Damage();
                }
            }
            Instantiate(grenadeExplodeVFXPrefab, targetPosition + Vector3.up * 1f, Quaternion.identity);
            trailRenderer.transform.parent = null;
            OnAnyGrenadeExploded?.Invoke(this, new EventArgs());
            Destroy(gameObject);
            onGrenadeBehaviourComplete();
        }
    } 

    // Public Methods
    // =============================================================================================

    public void Setup(GridPosition targetGridPosition, Action onGrenadeBehaviourComplete) {
        this.onGrenadeBehaviourComplete = onGrenadeBehaviourComplete;
        targetPosition = LevelGrid.Instance.GetWorldPosition(targetGridPosition);

        positionXZ = transform.position;
        positionXZ.y = 0;
        totalDistance = Vector3.Distance(positionXZ, targetPosition);


        Keyframe[] ks = new Keyframe[2];

        ks[0] = new Keyframe(0, 1);
        ks[0].outTangent = totalDistance / 4f;

        ks[1] = new Keyframe(1, 0);
        ks[1].inTangent = totalDistance / -2f;

        arcYAnimationCurve = new AnimationCurve(ks);
    }
}
