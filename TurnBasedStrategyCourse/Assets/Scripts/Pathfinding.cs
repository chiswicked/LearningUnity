using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Pathfinding : MonoBehaviour {
    public static Pathfinding Instance { get; private set; }

    [SerializeField] private Transform gridDebugObjectPrefab;
    [SerializeField] private LayerMask obstaclesLayerMask;

    public const int MOVE_COST_MULTIPLIER = 10;
    private const int MOVE_STRAIGHT_COST = 10;
    private const int MOVE_DIAGONAL_COST = 14;

    private GridSystem<PathNode> gridSystem;

    private int width;
    private int height;
    private float cellSize;

    // Lifecycle Event Handlers
    // =============================================================================================

    private void Awake() {
        if (Instance != null) {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    // Public Methods
    // =============================================================================================

    public void Setup(int width, int height, float cellSize) {
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;

        gridSystem = new GridSystem<PathNode>(width, height, cellSize,
            (GridSystem<PathNode> g, GridPosition gridPosition) => new PathNode(gridPosition));
        //gridSystem.CreateDebugObjects(gridDebugObjectPrefab);

        for (int x = 0; x < width; x++) {
            for (int z = 0; z < height; z++) {
                GridPosition gridPosition = new GridPosition(x, z);
                Vector3 worldPosition = LevelGrid.Instance.GetWorldPosition(gridPosition);
                float raycastOffsetDistance = 5f;
                bool isNodeWalkable = !Physics.Raycast(
                    worldPosition + Vector3.down * raycastOffsetDistance,
                    Vector3.up,
                    raycastOffsetDistance * 2,
                    obstaclesLayerMask
                );
                GetNodeByGridPosition(new GridPosition(x, z)).SetIsWalkable(isNodeWalkable);
            }
        }
    }

    public List<GridPosition> FindPath(GridPosition startGridPosition, GridPosition endGridPosition, out int pathLength) {
        List<PathNode> openList = new List<PathNode>();
        List<PathNode> closedList = new List<PathNode>();

        PathNode startNode = gridSystem.GetGridObject(startGridPosition);
        PathNode endNode = gridSystem.GetGridObject(endGridPosition);
        if (!endNode.IsWalkable()) {
            pathLength = 0;
            return null;
        }

        openList.Add(startNode);

        for (int x = 0; x < gridSystem.GetWidth(); x++) {
            for (int z = 0; z < gridSystem.GetHeight(); z++) {
                GridPosition gridPosition = new GridPosition(x, z);
                PathNode pathNode = gridSystem.GetGridObject(gridPosition);

                pathNode.SetGCost(int.MaxValue);
                pathNode.SetHCost(0);
                pathNode.CalculateFCost();
                pathNode.ResetParentNode();
            }
        }

        startNode.SetGCost(0);
        startNode.SetHCost(CalculateDistance(startGridPosition, endGridPosition));
        startNode.CalculateFCost();

        while (openList.Count > 0) {
            PathNode currentNode = GetLowestGCostPathNode(openList);
            if (currentNode == endNode) {
                pathLength = endNode.GetFCost();
                return CalculatePath(endNode);
            }

            openList.Remove(currentNode);
            closedList.Add(currentNode);

            foreach (PathNode neighbourNode in GetNeighbourNodeList(currentNode)) {
                if (closedList.Contains(neighbourNode)) continue;

                if (!neighbourNode.IsWalkable()) {
                    closedList.Add(neighbourNode);
                    continue;
                }

                int tenativeGCost =
                    currentNode.GetGCost() +
                    CalculateDistance(currentNode.GetGridPosition(), neighbourNode.GetGridPosition());
                if (tenativeGCost < neighbourNode.GetGCost()) {
                    neighbourNode.SetParentNode(currentNode);
                    neighbourNode.SetGCost(tenativeGCost);
                    neighbourNode.SetHCost(CalculateDistance(neighbourNode.GetGridPosition(), endGridPosition));
                    neighbourNode.CalculateFCost();

                    if (!openList.Contains(neighbourNode)) {
                        openList.Add(neighbourNode);
                    }
                }
            }
        }

        pathLength = 0;
        return null;
    }

    public bool IsWalkableGridPosition(GridPosition gridPosition) =>
        gridSystem.GetGridObject(gridPosition).IsWalkable();

    public void SetIsWalkableGridPosition(GridPosition gridPosition, bool isWalkable) =>
        gridSystem.GetGridObject(gridPosition).SetIsWalkable(isWalkable);

    public bool IsReachableGridPosition(GridPosition startGridPosition, GridPosition endGridPosition) =>
        FindPath(startGridPosition, endGridPosition, out int pathLength) != null;

    public int GetPathLength(GridPosition startGridPosition, GridPosition endGridPosition) {
        FindPath(startGridPosition, endGridPosition, out int pathLength);
        return pathLength;
    }

    // Private Methods
    // =============================================================================================

    private int CalculateDistance(GridPosition a, GridPosition b) {
        GridPosition gridPositionDistance = a - b;
        int xDinstance = Mathf.Abs(gridPositionDistance.x);
        int zDinstance = Mathf.Abs(gridPositionDistance.z);
        int straightDistance = Mathf.Abs(xDinstance - zDinstance);
        int diagonalDistance = Mathf.Min(xDinstance, zDinstance);
        return straightDistance * MOVE_STRAIGHT_COST + diagonalDistance * MOVE_DIAGONAL_COST;
    }

    private PathNode GetLowestGCostPathNode(List<PathNode> pathNodeList) {
        PathNode lowestGCostPathNode = pathNodeList[0];
        foreach (PathNode pathNode in pathNodeList) {
            if (pathNode.GetGCost() < lowestGCostPathNode.GetGCost()) {
                lowestGCostPathNode = pathNode;
            }
        }
        return lowestGCostPathNode;
    }

    private PathNode GetNodeByGridPosition(GridPosition gridPosition) =>
        gridSystem.GetGridObject(gridPosition);

    private List<PathNode> GetNeighbourNodeList(PathNode node) {
        List<PathNode> neigbours = new();

        GridPosition pos = node.GetGridPosition();
        GridPosition[] neigboursPositions = {
            pos + GridPosition.UP,
            pos + GridPosition.UP + GridPosition.RIGHT,
            pos + GridPosition.RIGHT,
            pos + GridPosition.RIGHT + GridPosition.DOWN,
            pos + GridPosition.DOWN,
            pos + GridPosition.DOWN + GridPosition.LEFT,
            pos + GridPosition.LEFT,
            pos + GridPosition.LEFT + GridPosition.UP,
        };

        foreach (GridPosition p in neigboursPositions)
            if (gridSystem.IsValidGridPosition(p))
                neigbours.Add(GetNodeByGridPosition(p));

        return neigbours;
    }

    private List<GridPosition> CalculatePath(PathNode endNode) {
        List<GridPosition> path = new List<GridPosition> { endNode.GetGridPosition() };
        PathNode parentNode = endNode.GetParentNode();
        while (parentNode != null) {
            path.Add(parentNode.GetGridPosition());
            parentNode = parentNode.GetParentNode();
        }
        path.Reverse();
        return path;
    }
}
