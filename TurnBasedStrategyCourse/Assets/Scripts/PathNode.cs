using System;
using UnityEngine;

public class PathNode {
    private GridPosition gridPosition;

    private int gCost;
    private int hCost;
    private int fCost;
    private PathNode parentNode;
    private bool isWalkable = true;

    // Constructor
    // =============================================================================================

    public PathNode(GridPosition gridPosition) {
        this.gridPosition = gridPosition;
    }

    // Public Methods
    // =============================================================================================

    public GridPosition GetGridPosition() => gridPosition;

    public int GetGCost() => gCost;
    public void SetGCost(int value) => gCost = value;

    public int GetHCost() => hCost;
    public void SetHCost(int value) => hCost = value;

    public int GetFCost() => fCost;

    public void CalculateFCost() => fCost = gCost + hCost;

    public PathNode GetParentNode() => parentNode;

    public void SetParentNode(PathNode parentNode) => this.parentNode = parentNode;

    public void ResetParentNode() => parentNode = null;

    public bool IsWalkable() => isWalkable;

    public void SetIsWalkable(bool walkable) => this.isWalkable = walkable;

    public override string ToString() {
        return gridPosition.ToString();
    }
}
