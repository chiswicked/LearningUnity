using System;
using UnityEngine;

public class TurnSystem : MonoBehaviour {
    public static TurnSystem Instance { get; private set; }

    public event EventHandler OnTurnChanged;

    private int turnNumber = 1;
    private bool isPlayerTurn = true;

    private void Awake() {
        if (Instance != null) {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    public void NextTurn() {
        turnNumber++;
        isPlayerTurn = !isPlayerTurn;

        OnTurnChanged?.Invoke(this, new EventArgs());
    }

    public int GetCurrentTurn() => turnNumber;

    public bool IsPlayerTurn() => isPlayerTurn;

    public bool IsEnemyTurn() => !isPlayerTurn;
}
