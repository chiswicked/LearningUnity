using Cinemachine;
using UnityEngine;

public class CameraController : MonoBehaviour {
    [SerializeField] private CinemachineVirtualCamera cinemachineVirtualCamera;
    private CinemachineTransposer cinemachineTransposer;

    [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private float rotationSpeed = 50f;
    [SerializeField] private float zoomSpeed = 10f;

    private float zoomOffset = 0f;

    private const float MIN_ZOOM_OFFSET = -4f;
    private const float MAX_ZOOM_OFFSET = +4f;

    private const float DEFAULT_OFFSET_Y = +7f;
    private const float DEFAULT_OFFSET_Z = -10f;

    //private Vector3 currentZoomOffset = Vector3.zero;
    private Vector3 targetZoomOffset = Vector3.zero;

    // Lyfecycle Event Handlers
    // =============================================================================================

    private void Start() {
        cinemachineTransposer = cinemachineVirtualCamera.GetCinemachineComponent<CinemachineTransposer>();
        targetZoomOffset = cinemachineTransposer.m_FollowOffset;
    }

    void Update() {
        HandleMovement();
        HandleRotation();
        HandleZoom();
    }

    // Private Methods
    // =============================================================================================

    private void HandleMovement() {
        Vector2 inputMoveDirection = InputManager.Instance.GetCameraMoveVector();
        Vector3 moveVector = transform.forward * inputMoveDirection.y + transform.right * inputMoveDirection.x;
        transform.position += moveVector * moveSpeed * Time.deltaTime;
    }

    private void HandleRotation() {
        Vector3 rotationVector = new Vector3(0, 0, 0);
        rotationVector.y = InputManager.Instance.GetCameraRotateAmount();
        transform.eulerAngles += rotationVector * rotationSpeed * Time.deltaTime;
    }

    private void HandleZoom() {

        float zoomValue = InputManager.Instance.GetCameraZoomAmount();
        zoomOffset -= zoomValue;
        zoomOffset = Mathf.Clamp(zoomOffset, MIN_ZOOM_OFFSET, MAX_ZOOM_OFFSET);
        targetZoomOffset = new Vector3(0, DEFAULT_OFFSET_Y - zoomOffset, DEFAULT_OFFSET_Z + zoomOffset);

        cinemachineTransposer.m_FollowOffset = Vector3.Lerp(cinemachineTransposer.m_FollowOffset, targetZoomOffset, Time.deltaTime * zoomSpeed);
    }
}
